
return new 'spec' {
  speed = 300,
  view = {
    color = { .2, .2, .7 },
    type = 'polygon',
    params = { 'fill',  -10, 5, 10, 5, 0, -15 }
  }
}

