
local Entity = new(Object) {
  spec = nil
}

function Entity.load(name)
  return new(Entity) {
    spec = new 'spec' (require('database.' .. name))
  }
end

function Entity:init()
  self.pos = self.pos or new(Vec) { 0, 0 }
  self.dir = new(Vec) { 0, 0 }
end

function Entity:setDirection(dir)
  self.dir = dir:normalized()
end

function Entity:update(dt)
  self.pos:translate(self.dir * self.spec.speed * dt)
end

function Entity:draw()
  local view = self.spec.view
  local g = love.graphics
  g.push()
  g.translate(self.pos:get())
  g.setColor(view.color)
  g[view.type](unpack(view.params))
  g.pop()
end

return Entity

