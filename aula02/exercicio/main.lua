
local _player
local _polygon
local _asteroids

function love.load()
  local W,H = love.graphics.getDimensions()
  _player = {
    speed = 300,
    x = W/2,
    y = H/2,
    w = 32,
    h = 32,
    texture = love.graphics.newImage("goblin spritesheet calciumtrice.png"),
    quads = {},
    frame = 1,
    animation = { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 },
    t = 0
  }
  _player.texture:setFilter('nearest', 'nearest')
  for i=0,9 do
    for j=0,9 do
      _player.quads[1 + i*10 + j] = love.graphics.newQuad(
        j*_player.w, i*_player.h, _player.w, _player.h,
        _player.texture:getDimensions()
      )
    end
  end

  _asteroid = {
    x = 0, y = 0,
    current = 1,
    sound = love.audio.newSource(
      "Juhani Junkala [Retro Game Music Pack] Title Screen.ogg", 'stream'
    ),
    progress = 0,
    path = require 'database.pathA',
    polygon = require 'database.polygonA'
  }
  _asteroid.sound:setLooping(true)
  _asteroid.sound:play()
end

function love.keypressed(key)
  if key == 'escape' then
    love.event.quit()
  end
end

local function _updateAsteroid(asteroid, dt)
  asteroid.progress = asteroid.progress + dt
  local path = asteroid.path
  if asteroid.progress > 1.0 then
    asteroid.progress = asteroid.progress - 1.0
    asteroid.current = asteroid.current % (#path/2) + 1
  end
  local current = asteroid.current
  local after = current % (#path/2) + 1
  local progress = asteroid.progress
  
  local prevx, prevy = unpack(path, current * 2 - 1, current * 2)
  local nextx, nexty = unpack(path, after * 2 - 1, after * 2)
  asteroid.x = prevx * (1 - progress) + nextx * progress
  asteroid.y = prevy * (1 - progress) + nexty * progress
  asteroid.sound:setPosition(asteroid.x - _player.x, asteroid.y - _player.y)
end

function love.update(dt)
  local dirx, diry = 0, 0
  if love.keyboard.isDown('up') then
    diry = -1
  end
  if love.keyboard.isDown('down') then
    diry = 1
  end
  if love.keyboard.isDown('left') then
    dirx = -1
  end
  if love.keyboard.isDown('right') then
    dirx = 1
  end
  _player.x = _player.x + _player.speed * dirx * dt
  _player.y = _player.y + _player.speed * diry * dt

  -- Update animation
  _player.t = _player.t + dt
  _player.frame = 1 + math.floor(_player.t / 0.1) % #_player.animation

  _updateAsteroid(_asteroid, dt)
end

function love.draw()

  -- Debug player animation
  love.graphics.print("frame: " .. _player.frame, 10, 10)

  -- Draw player
  local x,y,w,h = _player.x, _player.y, _player.w, _player.h
  love.graphics.push()
  love.graphics.translate(x, y)
  love.graphics.draw(_player.texture, _player.quads[_player.animation[_player.frame]],
                     0, 0, 0, 4, 4, w/2, h/2)
  love.graphics.pop()

  -- Draw polygon
  love.graphics.push()
  love.graphics.translate(_asteroid.x, _asteroid.y)
  love.graphics.polygon('fill', _asteroid.polygon)
  love.graphics.pop()
end

