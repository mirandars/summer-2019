




local down = false


function love.update(dt)
  down = love.keyboard.isDown('space')
end

function love.keypressed(key)
  if key == 'escape' then
    love.event.quit()
  end
end

function love.draw()
  if down then
    love.graphics.print("ESPAÇO", 10, 10)
  end
end

