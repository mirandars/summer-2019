# Aula 02 (10/1/2019) - *interaction*

Nessa aula vamos aprender a desenvolver as funcionalidades interativas de um jogo.

## Tratamento da entrada [20 min]

Dispositivos de Interface Humana:

+ Teclado (keycode vs. scancode)
+ Mouse (mouse vs. cursor)
+ Controles (mapeamento de botões)
+ Touch (gestos)
+ Barômetro e acelerômetro (imprecisão)
+ Câmera (processamento de imagem)
+ Microfone (processamento de áudio)
+ etc.

### Entrada passiva

Programa consulta estado do dispositivo

```lua
if love.keyboard.isDown('space') then
  -- espaço está apertado, não sei a quanto tempo
end
```

### Entrada ativa

Programa é avisado e informado pelo dispositivo

```lua
function love.keypressed(key)
  if key == 'space' then
    -- espaço foi imediatamente apertado!
  end
end
```

## Renderização gráfica e animação [50 min]

1. Hardware
    1. Tela e pixels
    2. Placa de vídeo
2. Software
    1. Drivers
    2. Bibliotecas
3. Renderização
    1. Janelas (vem do sistema operacional ou gerenciador de janelas)
    2. Rasterização
    3. Projeção
    4. Envio de geometrias, texturas, cores e outros dados
    5. Pilha de transformações
    6. Shaders
4. Animação
    1. Objetos gráficos (possuem estado)
    2. Animação quadro a quadro
    3. Animação interpolada com *keyframes*
    4. Tweens

## Renderização sonora [30 min]

1. Hardware
    1. Ondas de pressão de ar
    2. Placa de som (*digital to analog converter*)
2. Software
    1. Drivers
    2. Bibliotecas
3. Renderização
    1. Canais
    2. Áudio amostrado
    3. Espacialização
    4. *Fading*
    5. Polifonia

## Exercício [100 min]

1. Fechar programa com ESCAPE
2. Retângulo que se move com setas
3. Sprite simples
4. Animação quadro-a-quadro
5. Polígonos data-driven
6. Movimentação interpolada
7. Ruído espacializado

## Leituras complementares

### [LÖVE's graphics module](https://love2d.org/wiki/love.graphics)

### [LÖVE's audio module](https://love2d.org/wiki/love.audio)


