# Aula 01 (7/1/2019) - *the omega and the alfa*

Aqui vamos mostrar onde jogos começam e acabam, e como o curso vai ensinar isso.

## Apresentação do curso [40 min]

+ Apresentação dos instrutores
+ Apresentação dos alunos
+ Uso do LabX
+ Cronograma do curso

## Linha de produção de jogos [20 min]

Explicamos, partindo do jogo final, todo o processo necessário para se fazer um jogo.

![Pipeline](aula01/Aula01_Pipeline.jpg)

### Jogo final

É um programa, um executável, que fica no computador do jogador. Ele contém e precisa de bibliotecas, dados, assets, mods, arquivos de configuração e arquivos de save.

### A fonte

No computador do desenvolvedor estão os arquivos fonte do jogo, que variam de acordo com o papel exercido na equipe.

**Programador**: código fonte, bibliotecas e compilador

**Artista**: fonte dos assets e editores de assets

**Designer**: fonte dos dados e editor de dados

**Todos**: ferramentas de versionamento e ambiente de desenvolvimento integrado

### Entrega

Compilação, compressão e empacotamento. Adequação às diversas plataformas. Otimizações de desempenho e espaço.

## O Game Loop [40 min]

Jogos são programas que, em particular, são simulações temporais interativas.

![Game Loop](aula01/Aula01_GameLoop.jpg)

### Interação

Diferentemente de um programa de processamento de dados, programas interativos recebem entradas e produzem saídas várias vezes ao longo de sua execução, e cada saída provê as informações que o usuário precisa para realizar a próxima entrada. No caso de jogos, o ciclo de interação é feito em tempo-real, normalmente a 30 ou 60 ciclos por segundo.

### Simulação temporal

A cada interação com usuário, jogos computam o que mudou no mundo fictício do jogo. Ou seja, eles simulam a realidade do jogo. Mais ainda, essa simulação ocorre em passos devido à interatividade. A cada ciclo, um novo estado da simulação é computado.

### O Game Loop

Para obter tanto interatividade quanto processamento da simulação, todo jogo possui um game loop. É um laço que repete o ciclo entrada-simulação-saída até o jogo ser fechado pelo usuário. Todo o resto do código do jogo cresce a partir desse padrão arquitetural.

## Introdução à LÖVE (e Lua) [100 min]

Nesse curso usaremos o framework LÖVE pelas seguintes razões:

1. É código aberto (e, portanto, gratuito e acessível a todos)
2. É em Lua, uma linguagem simples e muito usada na indústria de jogos
3. Não tem tudo pronto, justamente para aprender como fazer cada parte do jogo
4. Mas tem pronto as partes mais chatas
5. O USPGameDev tem uma longa experiência com a ferramenta

Ela já deve estar instalada em todos as máquinas. Nós vamos usar a LÖVE versão 11.2.

### Usando a LÖVE

A LÖVE já é um programa executável por si só. Ele recebe os arquivos fonte de um jogo como entrada e executa o jogo em uma janela nova. Quando o jogo vai ser publicado, existem métodos para empacotar os arquivos fonte junto com o executável da LÖVE para que o jogador não precise instalar ela.

Para executar ela, basta evocar o comando no terminal:

```bash
$ love <caminho-para-pasta-do-jogo>
```

Em Windows, isso é feito arrastando a pasta e soltando-a sobre o executável da LÖVE.

### Estrutura de projeto

Quando executamos a LÖVE, ela trata o diretório passado de entrada como a raiz do projeto do jogo. Tudo ali dentro faz parte dele, tanto arquivos de código fonte e assets quanto qualquer outra coisa que você deixe lá, mesmo que não seja usado.

A LÖVE sempre procura por dois arquivos especiais logo na raiz do projeto, o main.lua e o conf.lua. Eles têm que ter exatamente esse nome, inclusive a mesma capitalização.

+ `main.lua`: onde definimos o game loop do jogo
+ `conf.lua`: onde definimos as configurações do jogo (ex: resolução da tela)

### Game Loop

A LÖVE já vem com uma implementação parcial de Game Loop. Essa implementação procura por funções definidas em main.lua para completar as partes que faltam do Game Loop. São elas:

+ `love.load(arg)`: chamada quando o jogo abre, e deve ser usada para carregar tudo que o jogo precisa para começar a rodar, recebe no parâmetro arg os argumentos da linha de comando
+ `love.update(dt)`: chamada todo ciclo do Game Loop para atualizar o estado da simulação temporal do jogo, recebe no parâmetro dt quanto tempo passou desde o último ciclo
+ `love.draw()`: chamada todo ciclo do Game Loop para produzir a saída gráfica do jogo, só é possível desenhar coisas na tela dentro dessa função

Essas funções que um framework espera que definamos e são disparadas automaticamente são chamadas de funções de callback. A LÖVE tem várias outras, principalmente para capturar a entrada do jogador, mas veremos isso em outra aula.

### Introdução relâmpago a Lua

Lua é uma linguagem interpretada ao invés de compilada. Ou seja, dentro da LÖVE tem uma máquina virtual que executa código em Lua ao invés do código ser executado direto pelo hardware do computador (advanced tip: mas LÖVE pode usar LuaJIT).

Sintaxe básica:

+ Existem oito tipos: nulo, números, booleanos, strings, funções, tabelas, userdata e co-rotinas
+ Variáveis possuem tipo dinâmico: o tipo delas é determinado pelo valor atual delas
+ Operações aritméticas: `+ - * / % ^`
+ Operações booleanas: `and, or, not`
+ Comentários: `-- assim`
+ Atribuição
  ```lua
  a, b = b, a  -- variáveis globais
  local x = 52 -- variáveis locais
  ```
+ Estruturas de controle
  ```lua
  if ... then ... elseif ... then ... else ... end
  while ... do ... end
  for i=m,n do ... end
  for ... in ... do ... end
  repeat ... until ...
  break
  ```
+ Funções são tipos de primeira ordem
  ```lua
  foo = function ( ... ) ... end
  function foo ( ... ) ... end -- Essa versão permite recursão
  ```
  + Recebem e devolvem qualquer quantidade de valores
+ Tabelas são estruturas de dados multi-uso
  + Servem como vetores (indexados a partir de 1)
    ```lua
	t = { 1, 2, 3 }
    t[1], t[2], t[3]
    for index,value in ipairs(t) do ... end
	```
  + Servem como tabelas associativas
    ```lua
	t = { a = 42, b = 1337 }
    t["a"], t["b"]
    for key,value in pairs(t) do ... end
	```
  + Servem como objetos (podem ter métodos)
    ```lua
	t.a = 42
    function t:foo( ... ) ... end
	```
  + `userdata` são referências para dados nativos, isto é, fora da VM do Lua
  + co-rotinas são para programação assíncrona

## Leituras complementares

### [Game Loop por Robert Nystrom](http://gameprogrammingpatterns.com/game-loop.html)

### [Programming in Lua (é para uma versão mais antiga de Lua, mas é bom mesmo assim)](https://www.lua.org/pil/contents.html)

### [LÖVE - Getting Started](https://love2d.org/wiki/Getting_Started)

### [LÖVE - Callback Functions](https://love2d.org/wiki/Tutorial:Callback_Functions)
